#include "simpletools.h"

#define USECPERTICK 1
#define bufLen 100 

#define _GAP 5000 
#define GAP_TICKS (_GAP / USECPERTICK)

#define TOLERANCE 25
#define LTOL (1.0 - TOLERANCE / 100.)
#define UTOL (1.0 + TOLERANCE / 100.)

#define TICKS_LOW(us) (int)(((us)*LTOL / USECPERTICK))
#define TICKS_HIGH(us) (int)(((us)*UTOL / USECPERTICK + 1))

// Pulse parms are *50-100 for the Mark and *50+100 for the space
// First MARK is the one after the long gap
// pulse parameters in usec
#define NEC_HDR_MARK 9000
#define NEC_HDR_SPACE 4500
#define NEC_BIT_MARK 560
#define NEC_ONE_SPACE 1600
#define NEC_ZERO_SPACE 560
#define NEC_RPT_SPACE 2250

#define NEC_BITS 32

// Decoded value for NEC when a repeat code is received
#define REPEAT 0xffffffff

#define MARK 0
#define SPACE 1

// Marks tend to be 100us too long, and spaces 100us too short
// when received due to sensor lag.
#define MARK_EXCESS 100

int
MATCH(int measured, int desired)
{
  return measured >= TICKS_LOW(desired) && measured <= TICKS_HIGH(desired);
}
int
MATCH_MARK(int measured_ticks, int desired_us)
{
  return MATCH(measured_ticks, (desired_us + MARK_EXCESS));
}
int
MATCH_SPACE(int measured_ticks, int desired_us)
{
  return MATCH(measured_ticks, (desired_us + MARK_EXCESS));
}

const uint32_t irPin = 15;

enum RcvState
{
  STATE_STOP,
  STATE_IDLE,
  STATE_MARK,
  STATE_SPACE,
};

volatile int* cog1;

bool decode(const uint32_t* packetBuf, const uint32_t packetLen,
            uint32_t& decodedResult);
bool printKey(const uint32_t resultCode);

static int
getMS(const uint32_t* irBuf, const uint32_t bufSize, const int offset)
{
  if (offset < 1)
    return -1;

  if (irBuf[offset - 1] > irBuf[offset])
    return -1;

  return (irBuf[offset] - irBuf[offset - 1]) / 80;
}

/* going to run this in a core to collect data */
void
collectData(void*)
{
  uint32_t curBufLen = 0;
  RcvState state = STATE_IDLE;
  uint32_t rawbuf[bufLen];

  simpleterm_open();
  printf("IR Decoder started...\n");

  while (1) {
    uint32_t spaceCnt = 0;
    volatile uint32_t irData = input(irPin);

    // check for buffer overun
    if (curBufLen > bufLen) {
      state = STATE_STOP;
      printf("Buffer Overrun %d", irData);
    }

    switch (state) {
      case STATE_IDLE:
        if (irData == MARK) {
          curBufLen = 0;
          rawbuf[curBufLen++] = CNT;
          state = STATE_MARK;
        }
        break;
      case STATE_MARK:
        if (irData == SPACE) {
          rawbuf[curBufLen++] = CNT;
          state = STATE_SPACE;
          spaceCnt = CNT;
        }
        break;
      case STATE_SPACE:
        if (irData == MARK) {
          rawbuf[curBufLen++] = CNT;

          state = STATE_MARK;
        } else {
          const uint32_t elapsedTime = (CNT - rawbuf[curBufLen - 1]) / 80;
          if (elapsedTime > GAP_TICKS) {
            state = STATE_STOP;
            uint32_t keyCode;
            decode(rawbuf, curBufLen, keyCode);
            curBufLen = 0;
            state = STATE_IDLE;
          }
        }
        break;
      case STATE_STOP:
        break;
    }
  }

  cog_end((int*)cog1);
}

bool
decode(const uint32_t* buf, const uint32_t packetLen, uint32_t& resultCode)
{
  int offset = 1;
  bool error = false;
  resultCode = 0;

  if (MATCH_MARK(getMS(buf, packetLen, offset), NEC_HDR_MARK)) {
    offset++;
    if (packetLen == 4 &&
        MATCH_SPACE(getMS(buf, packetLen, offset), NEC_RPT_SPACE) &&
        MATCH_MARK(getMS(buf, packetLen, offset + 1), NEC_BIT_MARK)) {
      resultCode = REPEAT;
    }

    if (packetLen < 2 * NEC_BITS + 4) {
      error = true;
    }

    // Initial space
    if (!MATCH_SPACE(getMS(buf, packetLen, offset), NEC_HDR_SPACE)) {
      error = true;
    }
    offset++;
    for (int i = 0; i < NEC_BITS; i++) {
      if (!MATCH_MARK(getMS(buf, packetLen, offset), NEC_BIT_MARK)) {
        error = true;
      }
      offset++;
      if (MATCH_SPACE(getMS(buf, packetLen, offset), NEC_ONE_SPACE)) {
        resultCode = (resultCode << 1) | 1;
      } else if (MATCH_SPACE(getMS(buf, packetLen, offset), NEC_ZERO_SPACE)) {
        resultCode <<= 1;
      } else {
        error = true;
      }
      offset++;
    }

    if (!error) {
      printKey(resultCode);
    }
  }
}

bool
printKey(const uint32_t resultCode)
{
  switch (resultCode) {
    case 0xFFA25D:
      printf("POWER");
      break;
    case 0xFFE21D:
      printf("FUNC/STOP");
      break;
    case 0xFF629D:
      printf("VOL+");
      break;
    case 0xFF22DD:
      printf("FAST BACK");
      break;
    case 0xFF02FD:
      printf("PAUSE");
      break;
    case 0xFFC23D:
      printf("FAST FORWARD");
      break;
    case 0xFFE01F:
      printf("DOWN");
      break;
    case 0xFFA857:
      printf("VOL-");
      break;
    case 0xFF906F:
      printf("UP");
      break;
    case 0xFF9867:
      printf("EQ");
      break;
    case 0xFFB04F:
      printf("ST/REPT");
      break;
    case 0xFF6897:
      printf("0");
      break;
    case 0xFF30CF:
      printf("1");
      break;
    case 0xFF18E7:
      printf("2");
      break;
    case 0xFF7A85:
      printf("3");
      break;
    case 0xFF10EF:
      printf("4");
      break;
    case 0xFF38C7:
      printf("5");
      break;
    case 0xFF5AA5:
      printf("6");
      break;
    case 0xFF42BD:
      printf("7");
      break;
    case 0xFF4AB5:
      printf("8");
      break;
    case 0xFF52AD:
      printf("9");
      break;
    case 0xFFFFFFFF:
      printf(" REPEAT");
      break;
  }
  printf("\n");
}

int
main()
{
  simpleterm_close();
  cog1 = cog_run(collectData, 512);

  return 0;
}